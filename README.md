# PeerTube HLS tv streaming

Adds support for streaming PeerTube HLS videos to Chromecast and Apple TV.


## Installation
1. Install latest (at least [3.3](https://github.com/Chocobozzz/PeerTube/releases/tag/v3.3.0)) PeerTube ([install guide](https://docs.joinpeertube.org/install-any-os)).
1. Activate HLS transcoding and disable web torrents.
1. Install this plugin.
