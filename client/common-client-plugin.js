import './style.scss'

const registerChromecastPlugin = require('@silvermine/videojs-chromecast')
const registerAirplayPlugin = require('@silvermine/videojs-airplay')

const GCAST_SCRIPT_STATUSES = {
  NOT_INITIATED: 0,
  INITIATED: 1,
  AVAILABLE: 2,
  NOT_AVAILABLE: 3,
}
let gcastScriptStatus = GCAST_SCRIPT_STATUSES.NOT_INITIATED

const waitForGcastScript = () => {
  let tries = 0;

  return new Promise((resolve, reject) => {
    const timer = setInterval(() => {
      if (gcastScriptStatus > GCAST_SCRIPT_STATUSES.INITIATED) {
        resolve();
        clearInterval(timer);
        return;
      }

      tries++;

      console.debug(`Waiting for gcastScriptExecuted`);

      if (tries > 25) {
        reject();
        clearInterval(timer);
      }
    }, 100);
  });
};

function register ({
  registerHook,
  peertubeHelpers,
}) {
  registerHook({
    target: 'action:video-watch.video.loaded',
    handler: async ({ videojs }) => {
      if (gcastScriptStatus > GCAST_SCRIPT_STATUSES.NOT_INITIATED) return;

      if (window.WebKitPlaybackTargetAvailabilityEvent) {
        registerAirplayPlugin(videojs)
      }

      window['__onGCastApiAvailable'] = (isAvailable) => {
        gcastScriptStatus = isAvailable ? GCAST_SCRIPT_STATUSES.AVAILABLE : GCAST_SCRIPT_STATUSES.NOT_AVAILABLE

        if (isAvailable) {
          registerChromecastPlugin(videojs, { preloadWebComponents: true })
        }
      };

      const script = document.createElement('script')
      script.src = 'https://www.gstatic.com/cv/js/sender/v1/cast_sender.js?loadCastFramework=1'

      gcastScriptStatus = GCAST_SCRIPT_STATUSES.INITIATED
      document.body.appendChild(script)
    },
  })

  registerHook({
    target: 'filter:internal.player.videojs.options.result',
    handler: opts => {
      return peertubeHelpers.getSettings()
        .then(async (settings) => {
          await waitForGcastScript();

          return settings;
        })
        .then((settings) => Promise.all([settings, opts])) // Fix PT v5 bug https://github.com/Chocobozzz/PeerTube/pull/5551
        .then(([settings, options]) => {
          if (gcastScriptStatus === GCAST_SCRIPT_STATUSES.AVAILABLE) {
            return Object.assign(
              {},
              options,
              {
                chromecast: {
                  modifyLoadRequestFn: function (loadRequest) {
                    loadRequest.media.hlsSegmentFormat = 'fmp4';
                    loadRequest.media.hlsVideoSegmentFormat = 'fmp4';
                    return loadRequest;
                  }
                },
                plugins: Object.assign(
                  {},
                  options.plugins,
                  {
                    chromecast: {
                      receiverAppID: settings['chromecast-receiver-app-id'] || undefined,
                    },
                  },
                  window.WebKitPlaybackTargetAvailabilityEvent ? { airPlay: {} } : {}
                ),
                techOrder: ['chromecast', 'html5'],
              }
            );
          }

          if (window.WebKitPlaybackTargetAvailabilityEvent) {
            return Object.assign(
              {},
              options,
              {
                plugins: Object.assign(
                  {},
                  options.plugins,
                  {
                    airPlay: {},
                  }
                ),
              }
            );
          }

          return options;
        });

    }
  })
}

export {
  register
}
