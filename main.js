async function register ({
  registerHook,
  registerSetting,
  settingsManager,
  storageManager,
  videoCategoryManager,
  videoLicenceManager,
  videoLanguageManager
}) {
  registerSetting({
    name: 'chromecast-receiver-app-id',
    label: 'Chromecast receiver app id',
    type: 'input',
    private: false,
  });
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
