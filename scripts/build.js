const path = require('path')
const esbuild = require('esbuild')
const { sassPlugin } = require('esbuild-sass-plugin')
const inlineImage = require('esbuild-plugin-inline-image')

const clientFiles = [
  'common-client-plugin.js'
]

const configs = clientFiles.map(f => ({
  entryPoints: [ path.resolve(__dirname, '..', 'client', f) ],
  bundle: true,
  minify: true,
  format: 'esm',
  target: 'safari11',
  plugins: [
    sassPlugin(),
    inlineImage(),
  ],
  outfile: path.resolve(__dirname, '..', 'dist', f),
}))

const promises = configs.map(c => esbuild.build(c))

Promise.all(promises)
  .catch(() => process.exit(1))
